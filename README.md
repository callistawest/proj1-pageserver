# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.
Using introductory software engineering tools such as server sockets to build response pages (errors or webpages) when the localhost is running.

### What is this repository for? ###

The objectives of this mini-project are:

  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)

* There are 3 scenarios when different pages will pop up according to the URL:

 (a) If URL ends with `name.html` or `name.css` (i.e., if `path/to/name.html` is in document path (from DOCROOT)), send content of `name.html` or `name.css` with proper http response.
 (b) If `name.html` is not in current directory Respond with 404 (not found).
 (c) If a page starts with one of the symbols(~ // ..), respond with 403 forbidden error. For example, `url=localhost:5000/..name.html` or `/~name.html` would give 403 forbidden error.
* URLs can be tested manually while the localhost is running using "make run" or using the test cases in the tests directory.

*I used Pythonexamples.org for a refresher on Python functions such as startswith() and endswith().  
  ```
  ## Author: Callista West, cwest10@uoregon.edu ##
  ```
